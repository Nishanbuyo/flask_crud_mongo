from flask import Flask,jsonify, request
from flask_pymongo import PyMongo
from bson.json_util import dumps
from bson.objectid import ObjectId


app = Flask(__name__)

app.secret_key = "SECRET_KEY"

app.config['MONGO_URI'] = "mongodb+srv://m001-student:m001-mongodb@sandbox.d8qcz.mongodb.net/mongoapi?retryWrites=true&w=majority"
mongo = PyMongo(app)

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not found'
    }
    res = jsonify(message)
    res.status_code = 404

    return res

@app.route('/', methods=['GET'])
def home():
    return "Hello World"

@app.route('/posts', methods=['GET'])
def show_posts():
    posts = mongo.db.posts.find()
    res = dumps(posts)
    return jsonify(res)

@app.route('/post/<id>')
def show_post(id):
    post = mongo.db.posts.find_one({'_id': ObjectId(id)})
    res = dumps(post)
    return res

@app.route('/add_post', methods=['POST'])
def add_post():
    data = request.get_json()
    userId = data['userId']
    title = data['title']
    body = data['body']

    if userId and title and body and request.method == 'POST':

        id = mongo.db.posts.insert(
            {'userId': userId, 'title': title, 'body': body})

        res = jsonify("post added successfully")
        res.status_code = 200

        return res

    else:
        return not_found()


@app.route('/post/update/<id>', methods=['PUT'])
def update_post(id):
    _id = id
    data = request.get_json()
    userId = data['userId']
    title = data['title']
    body = data['body']


    if userId and title and body and request.method == 'PUT':

        mongo.db.posts.update_one({'_id': ObjectId(_id['$oid']) if '$oid' in _id else ObjectId(
            _id)}, {'$set': {'userId': userId, 'title': title, 'body': body}})
        res = jsonify("post updated successfully")
        res.status_code = 200
        return res

    else:
        return not_found()


@app.route('/post/delete/<id>', methods=['DELETE'])
def delete_post(id):
    mongo.db.posts.delete_one({'_id': ObjectId(id)})
    res = jsonify("post deleted successfully")
    res.status_code = 200
    return res





if __name__ == '__main__':
    app.run(debug=True)
